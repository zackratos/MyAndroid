package com.zackratos.myandroid.demo02

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.zackratos.myandroid.BaseFragment
import com.zackratos.myandroid.databinding.FragmentDemo02Binding

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/16  5:47 PM
 * @Describe :
 */
class Demo02Fragment: BaseFragment() {

    private lateinit var binding: FragmentDemo02Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDemo02Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.toolbar.title = "demo02"
        val model: Demo02ViewModel by viewModels()
        model.observe(this.viewLifecycleOwner, Observer {
            binding.webView.loadDataWithBaseURL(null, it, "text/html", "utf-8", null)

        })

        binding.btnRequest.setOnClickListener { model.request2() }
    }


}