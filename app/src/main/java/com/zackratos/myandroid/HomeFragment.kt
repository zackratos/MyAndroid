package com.zackratos.myandroid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.zackratos.myandroid.databinding.FragmentHomeBinding

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/15  9:26 PM
 * @email    :
 * @Describe :
 */
class HomeFragment: BaseFragment() {

    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.setTitle(R.string.app_name)
        binding.btnDemo01.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.homeToDemo01())
        }
        binding.btnDemo02.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.homeToDemo02())
        }
    }
}