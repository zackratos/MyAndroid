package com.zackratos.myandroid.demo01

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.zackratos.myandroid.BaseFragment
import com.zackratos.myandroid.databinding.FragmentSafeArgsBinding

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/16  5:03 PM
 * @email    :
 * @Describe :
 */
class SafeArgsFragment: BaseFragment() {

    private lateinit var binding: FragmentSafeArgsBinding

    private val args: SafeArgsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSafeArgsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.title = "九阴真经"
        binding.tvContent.text = args.content
    }

}