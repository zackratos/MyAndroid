package com.zackratos.myandroid.demo01

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.zackratos.myandroid.BaseFragment
import com.zackratos.myandroid.R
import com.zackratos.myandroid.databinding.FragmentDemo01Binding

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/15  9:45 PM
 * @email    :
 * @Describe :
 */
class Demo01Fragment: BaseFragment() {

    private lateinit var binding: FragmentDemo01Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDemo01Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toolbar.title = "demo01"
        binding.btnSafeArgs.setOnClickListener {
//            findNavController().navigate(R.id.demo01_to_safe_args)
            findNavController().navigate(Demo01FragmentDirections.demo01ToSafeArgs("天之道，损有余而补不足"))
        }
    }



}