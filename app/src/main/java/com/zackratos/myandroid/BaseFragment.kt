package com.zackratos.myandroid

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.zackratos.ultimatebarx.library.UltimateBarX

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/16  1:18 PM
 * @email    :
 * @Describe :
 */
open class BaseFragment: Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initStatusBar()
        initNavigationBar()
    }

    protected fun initStatusBar() {
        UltimateBarX.with(this)
            .colorRes(R.color.deepSkyBlue)
            .fitWindow(true)
            .applyStatusBar()
    }

    protected fun initNavigationBar() {
        UltimateBarX.with(this)
            .colorRes(R.color.deepSkyBlue)
            .fitWindow(true)
            .applyNavigationBar()
    }

}