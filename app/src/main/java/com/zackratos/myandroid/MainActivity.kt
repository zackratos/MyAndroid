package com.zackratos.myandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.zackratos.myandroid.databinding.ActivityMainBinding
import com.zackratos.ultimatebarx.library.UltimateBarX

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ActivityMainBinding.inflate(layoutInflater).root)
        UltimateBarX.with(this).transparent().applyStatusBar()
        UltimateBarX.with(this).transparent().applyNavigationBar()
    }
}
