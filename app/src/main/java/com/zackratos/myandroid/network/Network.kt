package com.zackratos.myandroid.network

import retrofit2.Retrofit

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/16  8:13 PM
 * @Describe :
 */
class Network private constructor() {

    companion object {
        fun getInstance() = Holder.INSTANCE
    }

    private object Holder {
        val INSTANCE = Network()
    }

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://www.baidu.com/")
            .build()
    }

    val api: Api by lazy { retrofit.create(Api::class.java) }
}