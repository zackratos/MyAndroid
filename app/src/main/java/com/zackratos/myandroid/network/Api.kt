package com.zackratos.myandroid.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * @Author   : zhangwenchao
 * @Date     : 2020/12/16  8:12 PM
 * @Describe :
 */
interface Api {

    @GET
    fun baidu(@Url url: String = "https://www.baidu.com/"): Call<ResponseBody>

    @GET
    suspend fun baidu2(@Url url: String = "https://www.baidu.com/"): ResponseBody

}